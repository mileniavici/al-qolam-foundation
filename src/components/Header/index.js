import React from "react";
import Video from "../Video";
import "./style.css";

const Header = ({ title, subtitle, links = [], video_src = "" }) => {
  function renderLinks() {
    return links.map((link) => {
      return (
        <p>
          <a>{link}</a>
        </p>
      );
    });
  }
  return (
    <header className="header Merriweather">
      <div className="header__text_container">
        <div>
          <p className="header__title">{title}</p>
        </div>
        <div>
          <p className="header__subtitle">{subtitle}</p>
        </div>
        <div>{renderLinks()}</div>
        <div className="header__line_container"></div>
        <Video link={video_src} />
      </div>
    </header>
  );
};

export default Header;
