import React from "react";
import "./style.css";

const Video = (link) => {
  return (
    <div>
      <video src={link} muted="" loop="" autoPlay=""></video>
    </div>
  );
};

export default Video;
