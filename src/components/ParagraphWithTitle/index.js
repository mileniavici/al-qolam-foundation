import React from "react";
import "./style.css";

const ParagraphWithTitle = ({ title, paragraph, img_src }) => {
  return (
    <div className="paragraphTitle Merriweather">
      <div className="paragraphTitle__profileContainer">
        <p>{title}</p>
        <img src={img_src} />
      </div>
      <div className="paragraphTitle__sentenceContainer DarkParadise">
        <p>{paragraph}</p>
      </div>
    </div>
  );
};

export default ParagraphWithTitle;
