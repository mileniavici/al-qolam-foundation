import React from "react";

import Header from "../components/Header";
import MissionProject from "../components/MissionProject";
import NextProject from "../components/NextProject";
import SiapaKami from "../components/SiapaKami";
import VisionMission from "../components/VisionMission";

const Home = () => {
  return (
    <div>
      <Header
        title="Yayasan Islam Al-Qalam Amanah Ummat"
        subtitle="Institusi Pendidikan dan Riset Keagamaan"
        video_src=""
      />
      <SiapaKami />
      <MissionProject />
      <NextProject />
      <VisionMission />
    </div>
  );
};

export default Home;
