import React from "react";
import NavigationCard from "../NavigationCard";
import {
  lingkungan_binaan_contents,
  sekolah_inklusif_contents,
  sit_contents,
} from "./constants";
import "./style.css";

const MissionProject = () => {
  return (
    <div className="missionProject" id="our-projects">
      <div className="missionProject__header DarkParadise">
        <div className="missionProject__header__text">
          <p>OUR MISSION PROJECTS</p>
        </div>
      </div>

      <div className="missionProject__title Merriweather">
        <u>SEKOLAH ISLAM TERPADU</u>
      </div>

      <div className="missionProject__sit">
        <div className="missionProject__sit__left_image">
          <img src="/SIT_left.png" />
        </div>
        <div className="missionProject__sit__navCard">
          <NavigationCard
            title=""
            summaries={sit_contents}
            buttonTitle="PPDB SIT"
            buttonLink={"https://alqalam.ponpes.id/"}
          />
        </div>
        <div className="missionProject__sit__decoration">
          <div className="missionProject__sit__decoration__image">
            <img src="/navCard1.png" />
          </div>
        </div>
      </div>
      <div className="missionProject__lingkunganBinaan">
        <div className="missionProject__lingkunganBinaan__navCard">
          <NavigationCard
            title="LINGKUNGAN BINAAN"
            summaries={lingkungan_binaan_contents}
            buttonTitle="DONASI"
            buttonLink={"/kontak"}
          />
        </div>
        <div className="missionProject__lingkunganBinaan__decoration">
          <div className="missionProject__lingkunganBinaan__decoration__image">
            <img src="/navCard2.png" />
          </div>
        </div>
      </div>
      <div className="missionProject__sekolahInklusif">
        <div className="missionProject__sekolahInklusif___decoration">
          <div className="missionProject__sekolahInklusif__decoration__image">
            <img src="/navCard3.png" />
          </div>
        </div>
        <div className="missionProject__sekolahInklusif__navCard">
          <NavigationCard
            title="SEKOLAH INKLUSIF"
            summaries={sekolah_inklusif_contents}
            buttonTitle="DONASI"
            buttonLink={"/kontak"}
          />
        </div>
      </div>
    </div>
  );
};

export default MissionProject;
