import React from "react";
import { siapa_kami_contents } from "./constants";
import "./style.css";

const SiapaKami = () => {
  return (
    <React.Fragment>
      <div className="siapaKami" id="profil-yayasan">
        <div className="siapaKami__background">
          <div className="siapaKami__background__container">
            <div className="siapaKami__background__container__left Merriweather">
              <div className="siapaKami__background__container__left__title">
                <div>Siapa Kami</div>
              </div>
              <div className="siapaKami__background__container__left__more">
                {/* <a href="/profil-yayasan">Lebih lengkap</a> */}
              </div>
            </div>
            <div className="siapaKami__background__container__right DarkParadise">
              <p>{siapa_kami_contents}</p>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default SiapaKami;
