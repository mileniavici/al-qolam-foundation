import React from "react";
import { Link } from "react-router-dom";
import "./style.css";

const NavigationCard = ({ title, summaries, buttonTitle, buttonLink }) => {
  function renderSummary() {
    return summaries.map((el) => {
      return <React.Fragment>{el}</React.Fragment>;
    });
  }
  return (
    <React.Fragment>
      <div className="navCard">
        <div className="navCard__titleContainer Merriweather">
          <p>{title}</p>
        </div>
        <div className="navCard__textContainer DarkParadise">
          {renderSummary()}
        </div>
        <div className="navcard__button Merriweather">
          <a href={buttonLink} target="_blank">
            {buttonTitle}
          </a>
        </div>
      </div>
    </React.Fragment>
  );
};

export default NavigationCard;
