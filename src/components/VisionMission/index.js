import React from "react";
import "./style.css";

const VisionMission = () => {
  return (
    <div className="visionMission">
      <div className="visionMission__top__titleContainer DarkParadise">
        OUR VISION AND GOALS
      </div>
      <div className="visionMission__top">
        <div className="visionMission__top__left">
          <img src="image1.png" />
        </div>
        <div className="visionMission__top__right">
          <div className="visionMission__top__right__paragraph DarkParadise">
            Transforming education is a long journey, but we are committed to
            take the progress one step at the time. We believe any meaningful
            progress demands time, dedication, and precision. We are resolute in
            our mission to leave legacy to future generations.
          </div>

          <img className="visionMission__top__right__image" src="image2.png" />
        </div>
      </div>
      <div className="visionMission__center">
        <div className="visionMission__center__paragraph Merriweather">
          We are always able to bring our mission with strategies that improve
          education, reduce inequality, and create real values for society.
        </div>
      </div>
      <div className="visionMission__bottom">
        <img className="visionMission__bottom__image_left" src="image3.png" />
        <img className="visionMission__bottom__image_right" src="image4.png" />
      </div>
      <div className="visionMission__end">
        <img className="visionMission__end_img" src="image5.png" />
      </div>
    </div>
  );
};

export default VisionMission;
