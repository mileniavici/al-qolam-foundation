import React from "react";
import "./style.css";

const NextProject = () => {
  return (
    <div className="nextProject">
      <div className="nextProject__top">
        <div className="nextProject__top__textContainer DarkParadise">
          OUR NEXT PROJECT
        </div>
      </div>
      <div className="nextProject__middle">
        <div className="nextProject__middle__left">
          <div className="nextProject__middle__left__textContainer DarkParadise">
            “Maju atau mundurnya suatu kaum bergantung sebagian besar kepada
            pelajaran dan pendidikan dalam kalangan mereka itu. Tak ada suatu
            bangsa pun yang terbelakang menjadi maju melainkan sesudahnya
            mengadakan dan memperbaiki didikan anak-anak dan pemuda-pemuda
            mereka” - Muhammad Natsir
          </div>
        </div>
        <div className="nextProject__middle__right">
          <div className="nextProject__middle__right__textContainer DarkParadise">
            <p>Lokasi : Kab. Bima, Provinsi NTB </p>
            <p>Nama Proyek : Sekolah Islam Terpadu Al-Qalam Bima</p>
            <p>Jenis Proyek : Sekolah dan Asrama Tujuan</p>
            <p>Proyek : Pendidikan Gratis untuk Daerah Pelosok</p>
          </div>
        </div>
      </div>
      <div className="nextProject__bottom">
        <div className="nextProject__bottom__textContainer Merriweather">
          We are always able to bring our mission with strategies that improve
          education, reduce inequality, and create real values for society.
        </div>
      </div>
    </div>
  );
};

export default NextProject;
