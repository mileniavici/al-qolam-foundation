export const sit_contents = [
  "SIT Al-Qalam didirikan pada tahun 1996, diresmikan oleh Muhammad Natsir sebagai institusi pendidikan yang didasari dari nilai substansial dan esensial Islam yang kemudian diterjemahkan dalam kurikulum pembelajaran sebagai upaya menghasilkan produk budaya intelektual dari pencapaian spiritual.",
];

export const lingkungan_binaan_contents = [
  "Pesantren Al-Qalam ditargetkan untuk menangani permasalahan pendidikan dan moralitas yang dapat menjangkau kelas menengah bawah (dhuafa) serta yatim. Proyek ini juga berkontribusi dalam pengembangan kebangkitan cendikiawan muslim sehingga dapat menghasilkan suatu produk budaya intelektual dengan pencapaian spiritual keagamaan yang didasari dengan sumber kemurnian Islam.  ",
];

export const sekolah_inklusif_contents = [
  "Kabupaten Bima, Nusa Tenggara Barat secara statistik menempati jumlah penduduk miskin terbesar urutan ke 3, menempati urutan terendah pada indeks pembangunan manusia juga mengalami kekurangan SDM yang menyebarkan nilai-nilai pendidikan Islam . ",
  "Merespons isu kritis ini, sejak tahun 2007 hingga saat ini, kami telah berhasil memberi pendidikan gratis kepada ribuan anak pada kisaran usia kritis 12-15 tahun dengan memfasilitasi program Pendidikan di tingkat madrasah tsanawiyah. Lulusan siswa Al Qalam Bima ditargetkan memiliki kesadaran untuk ikut serta berkontribusi menjawab 3 tantangan isu kritis diatas.",
];
