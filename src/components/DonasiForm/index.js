import { useState } from "react";
import Form from "react-bootstrap/Form";
import "./style.css";

const DonasiForm = () => {
  const [namaLengkap, setNamaLengkap] = useState("");
  const [pesan, setPesan] = useState("");

  return (
    <div className="donasi Merriweather">
      <div className="donasi__title">
        <div className="donasi__title__textContainer">Kontak Kami</div>
      </div>

      <div className="donasi__subtitle">
        <div className="donasi__subtitle__textContainer">
          Kontak kami untuk pertanyaan atau berdonasi.
        </div>
      </div>
      <div className="donasi__form">
        <Form>
          <Form.Group className="donasi__form__name" controlId="nameInput">
            <div className="donasi__form__message__container">
              <Form.Label>Nama Lengkap: </Form.Label>
            </div>
            <div>
              <Form.Control
                type="nama"
                placeholder="Nama Anda"
                className="form-control-lg form-control-override"
                onChange={(e) => {
                  setNamaLengkap(e.target.value);
                }}
              />
            </div>
          </Form.Group>

          <Form.Group
            className="donasi__form__message"
            controlId="messageInput"
          >
            <div className="donasi__form__message__container">
              <Form.Label>Pesan: </Form.Label>
            </div>
            <div>
              <Form.Control
                as="textarea"
                rows={20}
                cols={225}
                placeholder="Pesan Anda"
                onChange={(e) => {
                  setPesan(e.target.value);
                }}
              />
            </div>
          </Form.Group>

          <div className="donasi__button__wa">
            <a
              href={
                "https://wa.me/+6281380692980?text=" +
                encodeURI("Halo, saya " + namaLengkap + ",\n" + pesan)
              }
            >
              Kirim Pesan Melalui WhatsApp
            </a>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default DonasiForm;
