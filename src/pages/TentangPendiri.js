import React from "react";

import Header from "../components/Header";
import ParagraphWithTitle from "../components/ParagraphWithTitle";
import { pendiri_content, pendiri_title } from "../fonts/contants";
import MisiPendiri from "../components/MisiPendiri";

const TentangPendiri = () => {
  return (
    <React.Fragment>
      <Header
        title="Tentang Pendiri"
        subtitle="Yayasan Islam Al-Qalam Amanah Ummat"
      />
      <ParagraphWithTitle
        title={pendiri_title}
        paragraph={pendiri_content}
        img_src={"pendiriYayasan.png"}
      />
      <MisiPendiri
        title={"Misi Pendiri"}
        main_image={"misiPendiriMainImage.png"}
        secondary_img_src={"misiPendiriSecondaryImage.png"}
      />
    </React.Fragment>
  );
};

export default TentangPendiri;
