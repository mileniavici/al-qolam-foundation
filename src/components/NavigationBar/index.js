import React, { useState } from "react";
import { HashLink } from "react-router-hash-link";
import { useLocation } from "react-router-dom";
import { logos, navbar_list } from "./constants";
import "./style.css";

const NavigationBar = () => {
  const [activePage, setActivePage] = useState("");
  const location = useLocation();
  function renderLogos() {
    return logos.map((logo_url, idx) => {
      return <img key={idx} src={logo_url} />;
    });
  }

  function renderNavItem() {
    return navbar_list.map((item, idx) => {
      return (
        <li
          id={location.pathname === item.key ? "navbar__active" : ""}
          key={idx}
          onClick={() => {
            setActivePage(item.title);
          }}
        >
          <HashLink smooth to={item.key}>
            {item.title}
          </HashLink>
        </li>
      );
    });
  }

  return (
    <nav className="navbar">
      <div className="navbar__logo_container">{renderLogos()}</div>
      <div className="navbar__link_container Merriweather">
        {renderNavItem()}
      </div>
    </nav>
  );
};
export default NavigationBar;
