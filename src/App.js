import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import TentangPendiri from "./pages/TentangPendiri";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import Home from "./pages/Home";
import Donasi from "./pages/Donasi";

function App() {
  return (
    <Router>
      <NavigationBar />
      <Routes>
        <Route name="home" index element={<Home />} />
        <Route path="/tentang-pendiri" element={<TentangPendiri />} />
        <Route path="/kontak" element={<Donasi />} />
      </Routes>
      <Footer />
    </Router>
  );
}

export default App;
