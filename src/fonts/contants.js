export const pendiri_content =
  "Muhammad Amin Djamaluddin merupakan pria kelahiran Bima, Nusa Tenggara Barat, memiliki riwayat lulusan pendidikan PGAN Bima selama 6 tahun. Muhammad Amin Djamaludin pernah menjabat sebagai salah satu staff ahli khusus dalam bidang keagamaan dari Muhammad Natsir saat Beliau menjabat sebagai Ketua Dewan Dakwah Islamiyah Indonesia.";
export const pendiri_title = "Pendiri Yayasan";
