import React from "react";
import { Link } from "react-router-dom";
import { footer_text, contact_info } from "./constants";
import "./style.css";

const Footer = () => {
  function renderText(contents) {
    return contents.map((el) => {
      return <div>{el}</div>;
    });
  }

  return (
    <footer className="footer Merriweather">
      <div className="footer__left">
        <div className="footer__left__sloganContainer">
          {renderText(footer_text)}
        </div>
        <div className="footer__left__linkContainer">
          <Link to="/tentang-pendiri">TENTANG PENDIRI</Link>
          <Link to={"/kontak"}>DONASI</Link>
        </div>
      </div>
      <div className="footer__right">
        <div className="footer__right__contactInfoContainer">
          {renderText(contact_info)}
        </div>
        <div className="footer__right__titleContainer">
          <p>Yayasan Islam Al-Qolam</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
