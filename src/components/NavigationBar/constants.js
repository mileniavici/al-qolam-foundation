export const logos = [
  "https://placehold.co/50x50",
  "https://placehold.co/50x50",
  "https://placehold.co/50x50",
  "https://placehold.co/50x50",
];

export const navbar_list = [
  {
    key: "/",
    title: "Home",
  },
  {
    key: "/#profil-yayasan",
    title: "Profil Yayasan",
  },
  {
    key: "/tentang-pendiri",
    title: "Tentang Pendiri",
  },
  {
    key: "/#our-projects",
    title: "Lingkup Pelayanan",
  },
  {
    key: "/kontak",
    title: "Donasi",
  },
];