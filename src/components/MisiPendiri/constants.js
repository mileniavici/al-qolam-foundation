export const first_paragraph =
  "Dari pengalaman yang terbentuk, Muhammad Amin Djamaludin mendapatkan manhaj dakwah yang sesuai dengan ruh dakwah Muhammad Natsir yang menjadikan tiga pilar sebagai basis dakwah pergerakan, yaitu: masjid, kampus dan pesantren. Jika dirunut sanad pemikiran dakwah Muhammad Amin Djamaludin, akan sampai pada pemikiran dakwah Muhammad Natsir; di mana memprioritaskan dakwah justru harus pada zona-zona tidak terjamah, atau kering dari dakwah dan penuh tantangan.";

export const second_paragraph =
  "Muhammad Amin Djamaluddin telah membangun dua bidang dakwah yaitu Binaa-an dan Difaa’an: Binaa-an dengan berdirinya Yayasan Islam Al-Qalam Amanah Ummat (YIQ-AU) dan Difaa’an dengan berdirinya Lembaga Penelitian dan Pengkajian Islam sebagai pencapaian aktualisasi cendikiawan untuk kebermanfataan ummat.";
