import React from "react";
import { first_paragraph, second_paragraph } from "./constants";
import "./style.css";

const MisiPendiri = ({ main_image, title, secondary_img_src }) => {
  return (
    <div className="misiPendiri Merriweather">
      <div className="misiPendiri__top">
        <div className="misiPendiri__top__left">
          <div className="misiPendiri__top__left__titleContainer Merriweather">
            <p>{title}</p>
          </div>
        </div>
        <div className="misiPendiri__top__right">
          <div className="misiPendiri__top__right__paragraphContainer DarkParadise">
            <p>{first_paragraph}</p>
          </div>
        </div>
      </div>
      <div className="misiPendiri__middle">
        <img src={main_image} />
      </div>
      <div className="misiPendiri__bottom">
        <div className="misiPendiri__bottom__left Merriweather">
          <p>{second_paragraph}</p>
        </div>
        <div className="misiPendiri__bottom__right">
          <img src={secondary_img_src} />
        </div>
      </div>
    </div>
  );
};

export default MisiPendiri;
