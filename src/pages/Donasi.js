import React from "react";
import DonasiForm from "../components/DonasiForm";
import Header from "../components/Header";

const Donasi = () => {
  return (
    <div>
      <Header
        title="Yayasan Islam Al-Qalam Amanah Ummat"
        subtitle="Institusi Pendidikan dan Riset Keagamaan"
        video_src=""
      />
      <DonasiForm />
    </div>
  );
};

export default Donasi;
