export const footer_text = [
  "Reinventing education is a long journey, but we are committed to take the progress one step at the time.",
  "We believe any meaningful progress demands time, dedication, and precision.",
  "We are resolute in our mission to leave legacy to future generations.",
];

export const contact_info = [
  "+62 813-8069-2980 ",
  "support@alqalam.ponpes.id ",
  "https://alqalam.ponpes.id/",
];
